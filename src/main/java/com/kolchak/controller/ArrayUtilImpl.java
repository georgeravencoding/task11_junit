package com.kolchak.controller;

import com.kolchak.model.MyArray;
import org.apache.logging.log4j.*;
import java.util.*;

public class ArrayUtilImpl implements ArrayUtil {
    private static Logger logger = LogManager.getLogger(ArrayUtilImpl.class);
    private MyArray myArray = new MyArray();

    @Override
    public void generateArray() {
        int[] mas = new int[myArray.getInts().length];
        for (int i = 0; i < myArray.getInts().length; i++) {
            mas[i] = (int) (Math.random() * 20);
        }
        myArray = new MyArray(mas);
    }

    @Override
    public void showArray() {
        logger.info(myArray);
    }

    @Override
    public void findValuesTask() {
        int[] mass = myArray.getInts();

        List<Integer> integerList1 = new ArrayList<>();
        for (int i = 0; i < mass.length; i++) {
            integerList1.add(mass[i]);
        }
        List<Integer> integerList2 = new ArrayList<>(integerList1);

        int count = 0;
        for (int i = 0; i < integerList1.size(); i++) {
            for (int j = 0; j < integerList2.size()-1; j++) {
                if (integerList1.get(i) == integerList2.get(j)) {
                    count++;
                }

            }
            System.out.print(integerList1.get(count) + " ");
        }
    }
}