package com.kolchak;

import com.kolchak.controller.ArrayUtilImpl;
import com.kolchak.view.MyView;

import org.apache.logging.log4j.*;

public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {

//        new MyView().show();

        ArrayUtilImpl arrayUtil = new ArrayUtilImpl();
        arrayUtil.generateArray();
        arrayUtil.showArray();
        arrayUtil.findValuesTask();
    }
}
