package com.kolchak.model;

import java.util.Arrays;

public class MyArray {
    int[] ints = new int[200];

    public MyArray() {
    }

    public MyArray(int[] ints) {
        this.ints = ints;
    }

    public int[] getInts() {
        return ints;
    }

    public void setInts(int[] ints) {
        this.ints = ints;
    }

    @Override
    public String toString() {
        return "MyArray{" +
                "ints=" + Arrays.toString(ints) +
                '}';
    }
}
